import frappe
from vanilla.patches.utils import update_custom_script


def execute():
    frappe.reload_doc('setup', 'doctype', 'authorization_rule')
    new_script = """
frappe.ui.form.on("Authorization Rule", {
    onload: function(frm) {
        frm.set_df_property("based_on", "read_only", frm.doc.transaction === "Expense Claim" ? 1 : 0);
    },
    transaction: function(frm) {
        if (frm.doc.transaction === 'Expense Claim') {
            frm.set_value("based_on", "Claim Amount");
            frm.set_df_property("based_on", "read_only", frm.doc.transaction === "Expense Claim" ? 1 : 0);
        } else if (frm.doc.transaction == 'Appraisal') {
            frm.set_value("based_on", "Not Applicable");
            frm.set_value("master_name", "");
            frm.set_value("system_role", "");
            frm.set_value("system_user", "");
            frm.set_value("value", 0);
            hide_field(['based_on', 'system_role', 'system_user', 'value']);
            unhide_field(['to_emp','to_designation']);
            frm.set_df_property("based_on", "read_only", frm.doc.transaction === "Expense Claim" ? 1 : 0);
        } else {
            unhide_field(['system_role', 'system_user','value', 'based_on']);
            hide_field(['to_emp','to_designation']);
            frm.set_df_property("based_on", "read_only", frm.doc.transaction === "Expense Claim" ? 1 : 0);
        }
    }
});
"""

    # Frappe names custom scripts as `[doctype-Client]`. As long as this
    # behavior does not change, we are safe
    if not frappe.db.exists('Custom Script', 'Authorization Rule-Client'):
        custom_script = frappe.new_doc('Custom Script')
        custom_script.dt = 'Authorization Rule'
    else:
        custom_script = frappe.get_doc(
            'Custom Script', 'Authorization Rule-Client')

    update_custom_script(custom_script, new_script)
