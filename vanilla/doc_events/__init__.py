"""
This package is for defining inbuilt `DocType` specific hooks.

The naming convention for modules in the package is `<doctype_name>.py`. For
example, if you are using `doc_events` to catch `on_submit` hook of `Expense
Claim`, you would add your code to `expense_claim.py`
"""
