import frappe
from frappe import _


def verify_authority(doc, *args):
    """
    Checks `Authorization Rule` and ensure that the approver is authorized to
    approve an `Expense Claim`.
    """
    expense_claim = doc

    possible_rules = get_applicable_authorization_rules(expense_claim)

    if not possible_rules:
        return
    elif not approver_can_approve(expense_claim, possible_rules):
        frappe.throw('{} is not an authorized approver'.format(
            expense_claim.exp_approver))


def get_applicable_authorization_rules(doc):
    """
    Returns Authorization Rules applicable to an Expense Claim.
    Authorization rule will be a tuple of tuple.
    The inner tuples will be in the form
    (
        (0). authorization value,
        (1). role needing authorization,
        (2). user needing authorization,
        (3). role that can approve,
        (4). user that can approve,
        (5). the 'based on' condition from the rule. It must be 'Claim Amount'
    ).

    The result will satisfy at least on of these conditions:
    1. The total claimed amount is less than the value in the Authorization
    Rule
    2. The approving user on the Expense Claim is same as that in the
    Authorization Rule
    3. The role of the approver on the Expense Claim is the same as that in the
    Authorization Rule
    """
    approver = doc.exp_approver
    approver_roles = frappe.get_roles(username=approver)
    amount = doc.total_claimed_amount

    return frappe.db.sql(
        """select value, system_role, system_user, approving_role,
        approving_user, based_on from `tabAuthorization Rule` where
        transaction="Expense Claim" and (
            value <= %s or approving_user = %s or approving_role in %s
        )
        order by value desc
        """,
        (amount, approver, approver_roles)
    )


def approver_can_approve(expense_claim, rules):
    """
    Returns True if the approver on an Expense Claim is allowed to approve the
    Expense Claim.

    `rules` will be a tuple of tuples
    The inner tuples will be in the form
    (
        (0). authorization value,
        (1). role needing authorization,
        (2). user needing authorization,
        (3). role that can approve,
        (4). user that can approve,
        (5). the 'based on' condition from the rule. It must be 'Claim Amount'
    ).
    """
    can_approve = False
    approver = expense_claim.exp_approver
    approver_roles = frappe.get_roles(username=approver)
    user = expense_claim.owner
    user_roles = frappe.get_roles(username=user)

    for rule in rules:
        # check if the rule applies to specific users
        if ((rule[1] in user_roles) or (rule[2] == user)) and \
                ((rule[3] in approver_roles) or (rule[4] == approver)):
            can_approve = True
        elif ((rule[3] in approver_roles) or (rule[4] == approver)):
            can_approve = True

        # this is enforced at the client but never trust the user
        if can_approve and rule[5] != 'Claim Amount':
            can_approve = False
            frappe.msgprint(
                _('The Authorization Rule is not based on claim amount. Please'
                    ' contact your Administrator.'))

        if can_approve:
            break

    return can_approve
