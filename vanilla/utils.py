import frappe


@frappe.whitelist()
def get_customer_balance(party):
    balance = frappe.db.sql(
        'select '
        'sum(credit_in_account_currency) - sum(debit_in_account_currency) '
        'from `tabGL Entry` '
        'where party_type = "Customer" and party=%s ', (party,))

    amount = frappe.utils.flt(balance[0][0]) if balance else 0

    return dict(balance=amount)
